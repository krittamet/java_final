import java.awt.Component;
import java.util.*;


public class Bag{


    private int Capacity;
    ArrayList<Item> Items ;
    

    public Bag(){
        Capacity = 10;
        Items = new ArrayList<Item>(Capacity);
        
    }
    
    public void addItem(){

        int range = (int)(Math.random()*2)+1;
        if(range==1){
            Items.add(new Item("HpPotion",60));
        }
        if(range==2){
            Items.add(new Item("MpPotion",60));
        }
        
    }

	public Item getItems(int i) {
		return Items.get(i);
    }
    public int sizeItems() {
		return Items.size();
    }
    public int getSizeBag(){
        return Capacity;
    }
    public void removeItem(int i){
        Items.remove(i);
    }
   
}