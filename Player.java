import java.util.*;
public class Player{
    String name;
    int exp;
    int hpPoint;
    int mpPoint;
    int damagePoint;
    String job;
    String jobPic;
    int level;
    int numKillMonster;

    int maxHpPoint;
    int maxMpPoint;

    Bag bag; 
    
    public Player(String Name,int hp,int mp,int damage,String setJob,String JobPic){
        name = Name;
        exp = 0;
        hpPoint = hp;
        mpPoint = mp;
        damagePoint = damage;

        maxHpPoint = hp;
        maxMpPoint = mp;  

        job = setJob;
        jobPic = JobPic;
        level = 1;
        bag = new Bag();
    }
    public void setName(String Name){
        name = Name;
    }

    public void setMp(int mp){
        mpPoint = mp;
    }
    public void setHp(int hp){
        hpPoint = hp;
    }
    public void setExp(int Exp){
        exp = Exp;
    }
    public void setLevel(int Level){
        level = Level;
    }
    public void setJobPic(String JobPic){
        jobPic = JobPic;
    }
    public void setNumKill(){
        numKillMonster = numKillMonster+1;
    }

    public int getNumKill(){
        return numKillMonster;
    }
    public int getMaxMp(){
        return maxMpPoint;
    }
    public int getMp(){
        return mpPoint;
    }
    public String getJobPic(){
        return jobPic;
    }
    public int getLevel(){
        return level;
    }
    public String getName(){
        return name;
    }
    public int getExp(){
        return exp;
    }
    public int getHp(){
        return hpPoint;
    }
    public int getDamage(){
        return damagePoint;
    }
    public int getMaxHp(){
        return maxHpPoint;
    }
    public String getJob(){
        return job;
    }

    

    public int usePotion(int index){
        return bag.getItems(index).getValue();
    }
    public int useSkill(){
        return 0;   
    }
    public int mpSkill() {
        return 0; 
    }
    
}