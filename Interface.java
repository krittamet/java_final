import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Interface extends JFrame{

    public Interface(Player player){

        super("Raknarok");
        
        //hp damage exp
        Monster slimeMons = new Monster(60,15,30);
        Monster elfMons = new Monster(80,20,50);
        Monster dragon1Mons = new Monster(100,25,70);
        Monster dragon2Mons = new Monster(120,35,80);

        Container c = getContentPane();
        c.setLayout(new BorderLayout(20,20));

        JPanel pNorth = new JPanel();
        JPanel pWest = new JPanel();
        JPanel pCenter = new JPanel();
        JPanel pEast = new JPanel();
        JPanel pSouth = new JPanel();

        //setLayout panel------------------------------------------------------------------------------------------------------

        pWest.setLayout(new BoxLayout(pWest,BoxLayout.Y_AXIS));

        pWest.setPreferredSize(new Dimension(150,400));

        pEast.setPreferredSize(new Dimension(150,400));

        pCenter.setPreferredSize(new Dimension(200,200));

        pNorth.setPreferredSize(new Dimension(900,50));

        pSouth.setPreferredSize(new Dimension(900,25));


        //North----------------------------------------------------------------------------------------------------------------
        JLabel name = new JLabel("Name :");
        JLabel showName = new JLabel();
        showName.setText(player.getName());
        showName.setFont(new Font("Serif", Font.BOLD, 18));
        name.setFont(new Font("Serif", Font.BOLD, 18));

        JLabel kill = new JLabel("            Kill :");
        JLabel showKill = new JLabel();
        showKill.setText(Integer.toString(player.getNumKill()));
        showKill.setFont(new Font("Serif", Font.BOLD, 18));
        kill.setFont(new Font("Serif", Font.BOLD, 18));

        //Add to NorthPanel

        pNorth.add(name);
        pNorth.add(showName);

        pNorth.add(kill);
        pNorth.add(showKill);

        //Center--------------------------------------------------------------------------------------------------------------

        pCenter.setLayout(new BoxLayout(pCenter,BoxLayout.Y_AXIS));

        JPanel pLevel = new JPanel();
        JPanel pExp = new JPanel();
        JPanel pHp = new JPanel();
        JPanel pMp = new JPanel();

        JLabel level = new JLabel("Level :");
        JLabel showLevel = new JLabel();
        showLevel.setText(Integer.toString(player.getLevel()));
        level.setFont(new Font("Serif", Font.BOLD, 18));
        showLevel.setFont(new Font("Serif", Font.BOLD, 18));
        
        JLabel myPlayer = new JLabel(new ImageIcon("player/"+player.getJobPic()));

        JLabel exp = new JLabel("Exp ");
        JProgressBar expBar = new JProgressBar();
        expBar.setMaximum(100);
        expBar.setMinimum(0);
        expBar.setValue(player.getExp());

        JLabel hp = new JLabel("HP ");
        JProgressBar hpBar = new JProgressBar();
        hpBar.setMaximum(player.getMaxHp());
        hpBar.setMinimum(0);
        hpBar.setValue(player.getHp());

        JLabel mp = new JLabel("Mp ");
        JProgressBar mpBar = new JProgressBar();
        mpBar.setMaximum(player.getMaxMp());
        mpBar.setMinimum(0);
        mpBar.setValue(player.getMp());

        pCenter.add(level);
        pCenter.add(showLevel);

        pLevel.add(level);
        pLevel.add(showLevel);
        pExp.add(exp);
        pExp.add(expBar);
        pHp.add(hp);
        pHp.add(hpBar);
        pMp.add(mp);
        pMp.add(mpBar);

        level.setBorder(BorderFactory.createEmptyBorder(0,10,0,0));
        pLevel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        pCenter.setBorder(BorderFactory.createEmptyBorder(100,100,100,100));
        myPlayer.setBorder(BorderFactory.createEmptyBorder(0,35,5,0));
        expBar.setBorder(BorderFactory.createEmptyBorder(0,0,0,5));

        pCenter.add(pLevel);
        pCenter.add(myPlayer);
        pCenter.add(pHp);
        pCenter.add(pMp);
        pCenter.add(pExp);
        
        //West-----------------------------------------------------------------------------------------------------------------

        JPanel gif1 = new JPanel();
        JPanel gif2 = new JPanel();
        JPanel gif3 = new JPanel();
        JPanel gif4 = new JPanel();

        gif1.setLayout(new BorderLayout(0,0));
        gif2.setLayout(new BorderLayout(0,0));
        gif3.setLayout(new BorderLayout(0,0));
        gif4.setLayout(new BorderLayout(0,0));

        JLabel slime = new JLabel(new ImageIcon("monster/slime.gif"));
        JLabel elf = new JLabel(new ImageIcon("monster/elf.gif"));
        JLabel dragon1 = new JLabel(new ImageIcon("monster/dragon.gif"));
        JLabel dragon2 = new JLabel(new ImageIcon("monster/dragon2.gif"));

        JProgressBar hpGif1 = new JProgressBar();
        JProgressBar hpGif2 = new JProgressBar();
        JProgressBar hpGif3 = new JProgressBar();
        JProgressBar hpGif4 = new JProgressBar();

        hpGif1.setPreferredSize(new Dimension(75,25));
        hpGif2.setPreferredSize(new Dimension(75,25));
        hpGif3.setPreferredSize(new Dimension(75,25));
        hpGif4.setPreferredSize(new Dimension(75,25));

        hpGif1.setMaximum(slimeMons.getMaxHpMonster());
        hpGif1.setMinimum(0);
        hpGif1.setValue(slimeMons.getHpMonster());

        hpGif2.setMaximum(elfMons.getMaxHpMonster());
        hpGif2.setMinimum(0);
        hpGif2.setValue(elfMons.getHpMonster());

        hpGif3.setMaximum(dragon1Mons.getMaxHpMonster());
        hpGif3.setMinimum(0);
        hpGif3.setValue(dragon1Mons.getHpMonster());

        hpGif4.setMaximum(dragon2Mons.getMaxHpMonster());
        hpGif4.setMinimum(0);
        hpGif4.setValue(dragon2Mons.getHpMonster());

        JButton atk1 = new JButton("atk");
        JButton atk2 = new JButton("atk");
        JButton atk3 = new JButton("atk");
        JButton atk4 = new JButton("atk");

        JPanel fight1 = new JPanel();
        JPanel fight2 = new JPanel();
        JPanel fight3 = new JPanel();
        JPanel fight4 = new JPanel();

        fight1.setLayout(new BoxLayout(fight1,BoxLayout.Y_AXIS));
        fight2.setLayout(new BoxLayout(fight2,BoxLayout.Y_AXIS));
        fight3.setLayout(new BoxLayout(fight3,BoxLayout.Y_AXIS));
        fight4.setLayout(new BoxLayout(fight4,BoxLayout.Y_AXIS));

        JButton skill1 = new JButton("skill");
        JButton skill2 = new JButton("skill");
        JButton skill3 = new JButton("skill");
        JButton skill4 = new JButton("skill");

        fight1.add(atk1);
        fight2.add(atk2);
        fight3.add(atk3);
        fight4.add(atk4);

        fight1.add(skill1);
        fight2.add(skill2);
        fight3.add(skill3);
        fight4.add(skill4);

        gif1.add(slime,BorderLayout.WEST);
        gif1.add(hpGif1,BorderLayout.SOUTH);
        gif1.add(fight1,BorderLayout.EAST);

        gif2.add(elf,BorderLayout.WEST);
        gif2.add(hpGif2,BorderLayout.SOUTH);
        gif2.add(fight2,BorderLayout.EAST);

        gif3.add(dragon1,BorderLayout.WEST);
        gif3.add(hpGif3,BorderLayout.SOUTH);
        gif3.add(fight3,BorderLayout.EAST);

        gif4.add(dragon2,BorderLayout.WEST);
        gif4.add(hpGif4,BorderLayout.SOUTH);
        gif4.add(fight4,BorderLayout.EAST);
        
        //Add to WestPanel

        pWest.add(gif1);
        pWest.add(gif2);
        pWest.add(gif3);
        pWest.add(gif4);

        gif1.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        gif2.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        gif3.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        gif4.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        //East---------------------------------------------------------------------------------------------------------------

        JButton Bag = new JButton("Bag");
        JButton changeName = new JButton("Change Name");
        pEast.add(Bag);
        pEast.add(changeName);

        //Add Panel to program------------------------------------------------------------------------------------------------
        c.add(pNorth,BorderLayout.NORTH);
        c.add(pWest,BorderLayout.WEST);
        c.add(pCenter,BorderLayout.CENTER);
        c.add(pEast,BorderLayout.EAST);
        c.add(pSouth,BorderLayout.SOUTH);

        //Add action --------------------------------------------------------------------------------------------------------

        atk1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            player.setHp(player.getHp()-slimeMons.getDamageMonster());
            hpBar.setValue(player.getHp());

            slimeMons.setHpMonster(slimeMons.getHpMonster()-player.getDamage());
            hpGif1.setValue(slimeMons.getHpMonster());

            if(hpGif1.getValue() == 0){

                slimeMons.setHpMonster(slimeMons.getMaxHpMonster());
                hpGif1.setValue(slimeMons.getMaxHpMonster());

                player.setExp(player.getExp()+slimeMons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
                
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
        });;

        atk2.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            player.setHp(player.getHp()-elfMons.getDamageMonster());
            hpBar.setValue(player.getHp());

            elfMons.setHpMonster(elfMons.getHpMonster()-player.getDamage());
            hpGif2.setValue(elfMons.getHpMonster());

            if(hpGif2.getValue() == 0){

                elfMons.setHpMonster(elfMons.getMaxHpMonster());
                hpGif2.setValue(elfMons.getMaxHpMonster());

                player.setExp(player.getExp()+elfMons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
        });;

        atk3.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            player.setHp(player.getHp()-dragon1Mons.getDamageMonster());
            hpBar.setValue(player.getHp());

            dragon1Mons.setHpMonster(dragon1Mons.getHpMonster()-player.getDamage());
            hpGif3.setValue(dragon1Mons.getHpMonster());

            if(hpGif3.getValue() == 0){

                dragon1Mons.setHpMonster(dragon1Mons.getMaxHpMonster());
                hpGif3.setValue(dragon1Mons.getMaxHpMonster());

                player.setExp(player.getExp()+dragon1Mons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
        });;

        atk4.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            player.setHp(player.getHp()-dragon2Mons.getDamageMonster());
            hpBar.setValue(player.getHp());

            dragon2Mons.setHpMonster(dragon2Mons.getHpMonster()-player.getDamage());
            hpGif4.setValue(dragon2Mons.getHpMonster());

            if(hpGif4.getValue() == 0){

                dragon2Mons.setHpMonster(dragon2Mons.getMaxHpMonster());
                hpGif4.setValue(dragon2Mons.getMaxHpMonster());

                player.setExp(player.getExp()+dragon2Mons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
        });;

        skill1.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            if(player.getMp()>player.mpSkill()){
            player.setHp(player.getHp()-slimeMons.getDamageMonster());
            hpBar.setValue(player.getHp());

            slimeMons.setHpMonster(slimeMons.getHpMonster()-player.useSkill());
            hpGif1.setValue(slimeMons.getHpMonster());

            mpBar.setValue(player.getMp());

            if(hpGif1.getValue() == 0){

                slimeMons.setHpMonster(slimeMons.getMaxHpMonster());
                hpGif1.setValue(slimeMons.getMaxHpMonster());

                player.setExp(player.getExp()+slimeMons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
            else JOptionPane.showMessageDialog(null,"NOT ENOUGH MANA");
        }
        });;
        skill2.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

            if(player.getMp()>player.mpSkill()){
            player.setHp(player.getHp()-elfMons.getDamageMonster());
            hpBar.setValue(player.getHp());

            elfMons.setHpMonster(elfMons.getHpMonster()-player.useSkill());
            hpGif2.setValue(elfMons.getHpMonster());

            mpBar.setValue(player.getMp());

            if(hpGif2.getValue() == 0){

                elfMons.setHpMonster(elfMons.getMaxHpMonster());
                hpGif2.setValue(elfMons.getMaxHpMonster());

                player.setExp(player.getExp()+elfMons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();
                
                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }
            else JOptionPane.showMessageDialog(null,"NOT ENOUGH MANA");
        }
            
        });;
        skill3.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

                if(player.getMp()>player.mpSkill()){
            player.setHp(player.getHp()-dragon1Mons.getDamageMonster());
            hpBar.setValue(player.getHp());

            dragon1Mons.setHpMonster(dragon1Mons.getHpMonster()-player.useSkill());
            hpGif3.setValue(dragon1Mons.getHpMonster());

            mpBar.setValue(player.getMp());

            if(hpGif3.getValue() == 0){

                dragon1Mons.setHpMonster(dragon1Mons.getMaxHpMonster());
                hpGif3.setValue(dragon1Mons.getMaxHpMonster());

                player.setExp(player.getExp()+dragon1Mons.getExpMonster());
                expBar.setValue(player.getExp());
                
                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
                showLevel.setText(Integer.toString(player.getLevel()));
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }else JOptionPane.showMessageDialog(null,"NOT ENOUGH MANA");
        }
        });;
        skill4.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){

                if(player.getMp()>player.mpSkill()){
            player.setHp(player.getHp()-dragon2Mons.getDamageMonster());
            hpBar.setValue(player.getHp());

            dragon2Mons.setHpMonster(dragon2Mons.getHpMonster()-player.useSkill());
            hpGif4.setValue(dragon2Mons.getHpMonster());

            mpBar.setValue(player.getMp());

            if(hpGif4.getValue() == 0){

                dragon2Mons.setHpMonster(dragon2Mons.getMaxHpMonster());
                hpGif4.setValue(dragon2Mons.getMaxHpMonster());

                player.setExp(player.getExp()+dragon2Mons.getExpMonster());
                expBar.setValue(player.getExp());

                player.bag.addItem();

                player.setNumKill();
                showKill.setText(Integer.toString(player.getNumKill()));
                
            }
            if(player.getExp()>100){
                player.setExp(player.getExp()-100);
                player.setLevel(player.getLevel()+1);
                expBar.setValue(player.getExp());
            }
            if(hpBar.getValue() == 0){
                JOptionPane.showMessageDialog(null,"YOU DIE");
                dispose();
            }
            }else JOptionPane.showMessageDialog(null,"NOT ENOUGH MANA");
        }
        });;
        Bag.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){
                dispose();
                new InterfaceBag(player);
        }
        });;
        changeName.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent e){
                String s = (String)JOptionPane.showInputDialog(null,"\"Input your name?\"","Customized Dialog",JOptionPane.PLAIN_MESSAGE,null,null,null);
                if ((s != null)) {
                    showName.setText(s);
                }
        }
        });;
        
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(900,600);
        setVisible(true);
        setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);

    }
    

}