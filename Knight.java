
public class Knight extends Player{

    public Knight(String name){

        // Name Hp Mp damage
        super(name,200,100,25,"Knight","Knight2.gif");
    }

    public int useSkill() {
        setMp(getMp()-mpSkill());
        return getDamage();
    }
    public int mpSkill() {
        
        return 30; 
    }
}