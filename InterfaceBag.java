import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;



public class InterfaceBag extends JFrame{

    ArrayList<JButton> buttons ;

    public InterfaceBag(Player player){

        buttons = new ArrayList<JButton>(player.bag.getSizeBag()); 

        Container b = getContentPane();
        b.setLayout(new FlowLayout());

        JPanel panel = new JPanel();

        for(int i=0; i < player.bag.sizeItems(); i++){
            buttons.add(new JButton(player.bag.getItems(i).getName()));
            panel.add(buttons.get(i));
            buttons.get(i).addActionListener(new UseItemInBag(player,i));
        }
        JButton exit = new JButton("Exit");
        panel.add(exit);
        b.add(panel);

        exit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                new Interface(player);
                dispose();
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();    
        setVisible(true);
        setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);
    }
    public class UseItemInBag implements ActionListener{
        
        int index;
        Player p1;
        public UseItemInBag(Player player,int i){
            index = i;
            p1 = player;
        }
        public void actionPerformed(ActionEvent e){
            if(p1.bag.getItems(index).getName().equals("HpPotion")){
                p1.setHp(p1.getHp()+p1.usePotion(index));
            }
            if(p1.bag.getItems(index).getName().equals("MpPotion")){
                p1.setMp(p1.getMp()+p1.usePotion(index));
            }
            
            p1.bag.removeItem(index);
            new Interface(p1);
            dispose();
        }
    }

    
}