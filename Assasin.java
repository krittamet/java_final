public class Assasin extends Player{

    public Assasin(String name){

        // Name Hp Mp damage
        super(name,180,100,30,"Assasin","Assasin2.gif");
    }

    public int useSkill() {
        setMp(getMp()-mpSkill());
        return getDamage(); 
    }
    public int mpSkill() {
        
        return 30; 
    }
}