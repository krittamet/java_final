

public class Archer extends Player{

    public Archer(String name){

        // Name Hp Mp damage
        super(name,150,100,35,"Assasin","Archer2.gif");
    }

    public int useSkill() {
        setMp(getMp()-mpSkill());
        return getDamage(); 
    }
    public int mpSkill() {
        
        return 30; 
    }
}