public class Item{

  private int value;
  private String nameItem;

  public Item(String name,int Value){
    nameItem = name;
    value = Value;
  }

  public int getValue(){
    return value;
  }
  public String getName(){
    return nameItem;
  }
}