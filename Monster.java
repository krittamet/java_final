
public class Monster{

    int hpMonster;
    int maxHpMonster;
    int damageMonster;
    int expMonster;

    public Monster(int hp,int damage,int exp){

        hpMonster = hp;
        maxHpMonster = hp;
        damageMonster = damage;
        expMonster = exp;
        
    }

    public void setHpMonster(int hp){
        hpMonster = hp;
    }
    public int getExpMonster(){
        return expMonster;
    }
    public int getHpMonster(){
        return hpMonster;
    }
    public int getMaxHpMonster(){
        return maxHpMonster;
    }
    public int getDamageMonster(){
        return damageMonster;
    }

}