import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Main extends JFrame{


    JLabel l1;
    JTextField f1;
    JButton b1;
    JButton b2;
    JButton b3;
    JCheckBox ch1;
    JCheckBox ch2;

    

    public Main(){
        
    super("Set Name");

    Container c = getContentPane();
    c.setLayout(new BoxLayout(c,BoxLayout.Y_AXIS));

    //Set name

    JPanel inputName = new JPanel();
    inputName.setLayout(new FlowLayout());

    l1 = new JLabel("Enter your Name : ");
    f1 = new JTextField(20);

    inputName.add(l1);
    inputName.add(f1);

    //Panel job 
    JPanel setJob = new JPanel();
    setJob.setLayout(new BoxLayout(setJob,BoxLayout.X_AXIS));

    //assasin
    JPanel assasin = new JPanel();
    assasin.setLayout(new BoxLayout(assasin,BoxLayout.Y_AXIS));

    JPanel assasin1 = new JPanel();
    JLabel assasin2 = new JLabel(new ImageIcon("player/Assasin.gif"));

    JPanel button1 = new JPanel();
    b1 = new JButton("Enter");

    JPanel assasin4 = new JPanel();
    JLabel assasin3 = new JLabel("Assasin");

    assasin4.add(assasin3);
    assasin1.add(assasin2);
    button1.add(b1);
    
    assasin.add(assasin4);
    assasin.add(assasin1);
    assasin.add(button1);
    

    //Knight
    JPanel knight = new JPanel();
    knight.setLayout(new BoxLayout(knight,BoxLayout.Y_AXIS));

    JPanel knight1 = new JPanel();
    JLabel knight2 = new JLabel(new ImageIcon("player/Knight.gif"));
    
    JPanel button2 = new JPanel();
    b2 = new JButton("Enter");
    
    JPanel knight4 = new JPanel();
    JLabel knight3 = new JLabel("Knight");

    knight4.add(knight3);
    knight1.add(knight2);
    button2.add(b2);

    knight.add(knight4);
    knight.add(knight1);
    knight.add(button2);

    //Archer
    JPanel archer = new JPanel();
    archer.setLayout(new BoxLayout(archer,BoxLayout.Y_AXIS));

    JPanel archer1 = new JPanel();
    JLabel archer2 = new JLabel(new ImageIcon("player/Archer.gif"));
    
    JPanel button3 = new JPanel();
    b3 = new JButton("Enter");

    JPanel archer4 = new JPanel();
    JLabel archer3 = new JLabel("Archer");

    archer4.add(archer3);
    archer1.add(archer2);
    button3.add(b3);

    archer.add(archer4);
    archer.add(archer1);
    archer.add(button3);

    //set job
    setJob.add(assasin);
    setJob.add(knight);
    setJob.add(archer);
     
    //set c
    c.add(inputName);
    c.add(setJob);

    //
    ClickListerner click = new ClickListerner();
    b1.addActionListener(click);
    b2.addActionListener(click);
    b3.addActionListener(click);

    //
    //b1.setBorder(BorderFactory.createEmptyBorder(0,5,0,0));
        
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setSize(500,300);     
    setVisible(true);
    setLocation((Toolkit.getDefaultToolkit().getScreenSize().width  - getSize().width) / 2, (Toolkit.getDefaultToolkit().getScreenSize().height - getSize().height) / 2);
    }

    public static void main(String[] args){      
        new Main();
    }

    public class ClickListerner implements ActionListener{

        public void actionPerformed(ActionEvent e){
            if(e.getSource() == b1){
                Player p1 = new Assasin(f1.getText());
                dispose();
                new Interface(p1);
            }
            if(e.getSource() == b2){
                Player p1 = new Knight(f1.getText());
                dispose();
                new Interface(p1);
            }
            if(e.getSource() == b3){
                Player p1 = new Archer(f1.getText());
                dispose();
                new Interface(p1);
            }
        }
    }
        
}
